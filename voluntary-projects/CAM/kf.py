import numpy as np

class KF():
    def __init__(self, A, H, R, Q):
        self.A = A
        self.H = H
        self.Q = Q
        self.R = R
        self.P = np.eye(A.shape[0]) * 1000.
        self.x = np.zeros(A.shape[0])
        self.n = A.shape[0]
        self.log_x = []

        
    def predict(self, u=None):
        xminus = self.A @ self.x 
        Pminus = self.A @ self.P @ self.A.T + self.Q
        self.x = xminus
        self.P = Pminus
        
    def update(self, y):
        invP = np.linalg.inv(self.P)
        invR = np.linalg.inv(self.R)
        Pplus = np.linalg.inv(invP + self.H.T @ invR @ self.H)
        xplus = Pplus @ (invP @ self.x.T + self.H.T @ invR @ y)
        self.x = xplus
        self.P = Pplus
        
    def log(self):
        self.log_x.append(self.x.copy())
